package tn.mss.eservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.mss.eservices.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
